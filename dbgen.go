package main

import (
    "fmt"

    "github.com/boltdb/bolt"
)

func DBGenerate() {

    base, err := bolt.Open("head.db", 0600, nil)
    if err != nil {
        fmt.Println(err)
    }

    err = base.Update(func(tx *bolt.Tx) error {
        _, err = tx.CreateBucketIfNotExists([]byte("First bucket"))
        if err != nil {
            return err
        }
        _, err = tx.CreateBucketIfNotExists([]byte("Second bucket"))
        if err != nil {
            return err
        }
        _, err = tx.CreateBucketIfNotExists([]byte("Third bucket"))
        if err != nil {
            return err
        }
        b := tx.Bucket([]byte("Second bucket"))
        err = b.Put([]byte("AKey"), []byte("AValue"))
        err = b.Put([]byte("BKey"), []byte("BValue"))
        _, err = b.CreateBucketIfNotExists([]byte("Interior bucket"))
        if err != nil {
            return err
        }
        z := b.Bucket([]byte("Interior bucket"))
        err = z.Put([]byte("One"), []byte("1"))
        if err != nil {
            return err
        }
        err = z.Put([]byte("Two"), []byte("2"))
        if err != nil {
            return err
        }
        t := tx.Bucket([]byte("Third bucket"))
        err = t.Put([]byte("3rd key A"), []byte("asper"))
        if err != nil {
            return err
        }
        err = t.Put([]byte("3rd key B"), []byte("quercus"))
        if err != nil {
            return err
        }
        return err
    })
    base.Close()

}
