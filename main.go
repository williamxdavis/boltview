package main

import (
    "fmt"
    "html/template"
    "net/http"
    "os"

    "github.com/boltdb/bolt"
)

type Dbdata struct {
    Buckets []string
    Keys []string
    //problem is these are unrelated
}

func bucketTree(bucket *bolt.Bucket) []string {
    a := []string{}
    if bucket == nil {
        return a
    }
    c := bucket.Cursor()
    for k, v := c.First(); k != nil; k, v = c.Next() {
        a = append(a, string(k) + " ---> " + string(v) + "\n")
        if v == nil {
            a = append(a, bucketTree(bucket.Bucket(k))...)
        }
    }
    return a
}

func handler(w http.ResponseWriter, r *http.Request) {
    items := infopipe(os.Args[1])
    t, err := template.ParseFiles("templates/index.tpl")
    err = t.Execute(w, items)
    if err != nil {
        fmt.Println(err)
    }
}

func serv() {
    server := http.Server{
        Addr: "127.0.0.1:9823",
    }
    http.HandleFunc("/", handler)
    fmt.Printf("Listening at %s... ", server.Addr)
    http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
    server.ListenAndServe()
}

func infopipe (db string) Dbdata {
    data := Dbdata{}
    base, err := bolt.Open(db, 0600, nil)
    if err != nil {
        fmt.Println(err)
    }
    e := base.Update(func(tx *bolt.Tx) error {
        c := tx.Cursor()
        for k, _ := c.First(); k != nil; k, _ = c.Next() {
            b := tx.Bucket(k)
            name := []string{string(k)}
            data.Buckets = append(data.Buckets, name...)
            data.Keys = append(data.Keys, bucketTree(b)...)
                        }
                return nil
            })
    if e != nil {
        fmt.Println(e)
    }
    base.Close()
    return data

}
func main() {
//    DBGenerate()
    serv()
}



    //vv put all this somewhere

//	keypair, err := secrethandshake.LoadSSBKeyPair("secret.json")
//    if err != nil {
//		keypair, err = secrethandshake.GenEdKeyPair(rand.Reader)
//		if err != nil {
//			log.Fatal(err)
//		}
//
//    datastore, _ := ssb.OpenDataStore("feeds.db", keypair)
//	if err != nil {
//		log.Fatal(err)
//	}
//	defer datastore.Close()
//
//
//
//func OpenDataStore(path string, primaryKey *secrethandshake.EdKeyPair) (*DataStore, error) {
//	db, err := bolt.Open(path, 0600, nil)
//	if err != nil {
//		return nil, err
//	}
//	ds := &DataStore{
//		db:        db,
//		feeds:     map[Ref]*Feed{},
//		Topic:     NewMessageTopic(),
//		extraData: map[string]interface{}{},
//		Keys:      map[Ref]Signer{},
//	}
//	ds.PrimaryKey = primaryKey
//	ds.PrimaryRef, _ = NewRef(RefFeed, ds.PrimaryKey.Public[:], RefAlgoEd25519)
//	ds.Keys[ds.PrimaryRef] = &SignerEd25519{ed25519.PrivateKey(ds.PrimaryKey.Secret[:])}
//
//	for _, im := range initMethods {
//		im(ds)
//	}
//
//	return ds, nil
//}
//
//
//import (
//	"encoding/binary"
//	"errors"
//	"fmt"
//	"log"
//	"sync"
//	"time"
//
//	"github.com/boltdb/bolt"
//	"github.com/cryptix/secretstream/secrethandshake"
//	"golang.org/x/crypto/ed25519"
//)
//
//
//
//
    //^^ put all this somewhere
